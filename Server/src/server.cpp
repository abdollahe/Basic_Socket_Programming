#include <iostream>
#include <string>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <future>
#include <thread>
#include <vector>
#include <algorithm>


// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
} ;

void communicationTask(int fd) {
    char clientNumber[1024] = {'0'} ;
    recv(fd , clientNumber , sizeof(clientNumber) , 0) ;
    std::cout << "Got name of client, its: " << clientNumber << std::endl ;
    
    std::cout << "Waiting for messages from async task\n" ;
    std::cout << "------------------------------------\n" ;

    char buffer[2048] = { 0 } ;
    std::string message ; 
    int dataLength = 0 ; 

    while(1) {
        // while(dataLength = recv(fd , buffer , sizeof(buffer) , 0) > 0) {
        //     message.append(buffer , dataLength) ;
        // }
        dataLength = recv(fd , buffer , sizeof(buffer) , 0) ;
        message.append(buffer , dataLength) ;
        std::cout << clientNumber <<" says: " << message << std::endl ;
        message.clear() ;
    }
    
} ;

int main(int argc , char *argv[]) {

    
    struct addrinfo hints, *addrInfoList;
    int status ;
    char ipstr[INET6_ADDRSTRLEN] ;

    int serverSocketFd = 0 ;
    int requesterSocketFd = 0 ;
    
    int yes=1;

    int SERVER_BACKLOG_QUEUE = 10 ;

    struct sockaddr_storage requestersInfo; // incoming client's address information

    // Create a vector of futures to hold references 
    // to the tasks that are to be created
    std::vector<std::future<void>> futures ;
    
    char ipAddress[16] = {'1' , '2' , '7' , '.' , '0', '.' , '0' , '.' , '1'} ;

    if(argc < 2) {
        std::cout << "No IP address supplied, using local device loop back address\n" ;
        strncpy(ipAddress , ipAddress , sizeof(ipAddress)) ;  
    }
    else {
        strncpy(ipAddress , argv[1] , sizeof(argv[1])) ;
    }

    memset(&hints , 0 , sizeof(hints)) ;
    // set internet address as IPv4.
    hints.ai_family = AF_INET ;
    // Set the protocol to TCP.
    hints.ai_socktype = SOCK_STREAM ; 
    
    status = getaddrinfo(ipAddress , "3490" , &hints , &addrInfoList) ;

    if (status != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
        return 2;
    }

    
    // setup the socekt
    if ((serverSocketFd = socket(addrInfoList->ai_family , addrInfoList->ai_socktype , addrInfoList->ai_protocol)) == -1) {
        perror("server: socket");
        return 3 ;
    }

    // set socket options
    if (setsockopt(serverSocketFd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
			perror("setsockopt");
			exit(1);
	}

    // Try to bind socket
    if (bind(serverSocketFd, addrInfoList->ai_addr, addrInfoList->ai_addrlen) == -1) {
			close(serverSocketFd);
			perror("server: bind");
			return 3 ;
            
    }

    // Start listening for connections 

    if (listen(serverSocketFd , SERVER_BACKLOG_QUEUE) == -1) {
        perror("listen");
		exit(1);
    }
    std::cout << "Listening for connections....\n" ;

    //while loop to accept any incoming requests and thread creation for all
    while(true) {
        socklen_t requesterAddrSize = sizeof(requestersInfo);
		requesterSocketFd = accept(serverSocketFd, (struct sockaddr *)&requestersInfo, &requesterAddrSize);
		if (requesterSocketFd == -1) {
			perror("accept");
			continue;
		}

		inet_ntop(requestersInfo.ss_family,
			get_in_addr((struct sockaddr *)&requestersInfo),
			ipstr, sizeof(ipstr));
		printf("server: got connection from %s\n", ipstr);

        // create a an async task to cimmunicate with the 
        futures.emplace_back(std::async(std::launch::async , communicationTask , requesterSocketFd)) ;

    }

    std::for_each(futures.begin() , futures.end() , [](std::future<void> & ftr) {
          ftr.wait() ;
    }) ;
    

    return 0 ;
}